package screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.AndroidServerFlag;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;

public class Mobile {
    private AppiumDriverLocalService appiumDriverLocalService;
    AppiumDriver<MobileElement> driver;

    private int seledroidPort;

    private void initSeledroidPort() {
        if (seledroidPort == 0)
            seledroidPort = getPort();
    }

    public int getSeledroidPort() {
        return seledroidPort;
    }

    public int getPort() {
        int port = 0;
        try (ServerSocket socket = new ServerSocket(0)) {
            socket.setReuseAddress(true);
            port = socket.getLocalPort();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return port;
    }

    @Test
    public void appiumAndroidServer() {
        System.out.println("Starting Android Appium Server");
        //MyLogger.info("Device UDID: " + deviceID);
        int port = getPort();
        int chromePort = getPort();
        int bootstrapPort = getPort();
        initSeledroidPort();
        int selendroidPort = getSeledroidPort();
        System.out.println("BootStrapPort" + bootstrapPort);
        AppiumServiceBuilder builder = new AppiumServiceBuilder()
                .withAppiumJS(new File("C:\\Users\\tanhp\\AppData\\Roaming\\npm\\node_modules\\appium\\build\\lib\\main.js"))
                .withArgument(GeneralServerFlag.LOG_LEVEL, "info")
                .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
                .withIPAddress("127.0.0.1")
                //.withLogFile(new File( MyConstants.APPIUM_LOGS_FOLDER +"[Android]_"+ deviceID + "__" + methodName + MyConstants.TXT_EXTENSION))
                .withArgument(AndroidServerFlag.CHROME_DRIVER_PORT, Integer.toString(chromePort))
                .withArgument(AndroidServerFlag.BOOTSTRAP_PORT_NUMBER, Integer.toString(bootstrapPort))
                .withArgument(AndroidServerFlag.SUPPRESS_ADB_KILL_SERVER)
                .withArgument(AndroidServerFlag.SELENDROID_PORT, Integer.toString(selendroidPort)).usingPort(port)
                .usingDriverExecutable(new File("C:\\Program Files\\nodejs\\node.exe"));
        this.appiumDriverLocalService = AppiumDriverLocalService.buildService(builder);
        this.appiumDriverLocalService.start();
        if (!this.appiumDriverLocalService.isRunning()) {
            System.out.println("Appium Android service can NOT started, please check it and try again!!!!");
        }
        System.out.println("Is Appium Driver Local Service is running: " + this.appiumDriverLocalService.isRunning());
        System.out.println("Appium service builder instance is:" + builder);
    }

    public URL getAppiumUrl() {
        return this.appiumDriverLocalService.getUrl();
    }

    private DesiredCapabilities androidNative() {
        System.out.println("Setting up capabilities for Android native device");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        //capabilities.setCapability("automationName", "uiautomator2");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "192.168.248.101:5555");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "4.4.4");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.sendoseller");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.sendoseller.MasterActivity");
        capabilities.setCapability(AndroidMobileCapabilityType.RESET_KEYBOARD,true);
        capabilities.setCapability(AndroidMobileCapabilityType.UNICODE_KEYBOARD,true);
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60");
        //capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        //capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
        return capabilities;
    }

    protected AppiumDriver<MobileElement> startAppiumServerInParallel() {
        AppiumDriver<MobileElement> driverToStart = null;
        DesiredCapabilities capabilities = androidNative();
        System.out.println("Appium URL with Android Platform: "+ getAppiumUrl());
        driverToStart = new AndroidDriver<>(getAppiumUrl(), capabilities);
        System.out.println("Android driver has started");
        return driverToStart;
    }

    public void waitForElementToBeVisible(By selector) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.presenceOfElementLocated(selector));
        } catch (Exception e) {
            throw new NoSuchElementException(String.format("The following element was not visible: %s", selector));
        }
    }



    @Test
    public void main() throws IOException {
        appiumAndroidServer();
        driver = startAppiumServerInParallel();
        MobileElement element = driver.findElement(By.id("com.sendoseller:id/btn_cancel"));
        element.click();

        MobileElement elem = driver.findElement(By.id("com.sendoseller:id/login"));

        org.openqa.selenium.Point point = elem.getCenter();
        int centerx = point.getX();
        int centerY = point.getY();

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        BufferedImage image = ImageIO.read(scrFile);
        // Getting pixel color by position x and y
        int clr=  image.getRGB(centerx,centerY);
        int  red   = (clr & 0x00ff0000) >> 16;
        int  green = (clr & 0x0000ff00) >> 8;
        int  blue  =  clr & 0x000000ff;
        System.out.println("Red Color value = "+ red);
        System.out.println("Green Color value = "+ green);
        System.out.println("Blue Color value = "+ blue);
        String hex = String. format("#%02x%02x%02x", red, green, blue);
        System.out.println(hex);

    }
}
