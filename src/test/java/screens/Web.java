package screens;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;
import utils.XLSWorker;

import java.util.List;

public class Web {
    XSSFWorkbook workbook;
    XSSFSheet sheet;
    WebDriver driver;
    String sheetName = "Register";

    private String getText(By by){
        try{
            WebElement element = driver.findElement(by);
            return element.getText();
        }catch (Exception e){
            return null;
        }

    }

    @Test
    public void runZeplin(){
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/driver/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);

        workbook = XLSWorker.getWorkbook(System.getProperty("user.dir") + "/input/input_css.xlsx");
        sheet = XLSWorker.getSheet(workbook, sheetName);

        try{
            JavascriptExecutor js = (JavascriptExecutor)driver;
            driver.get("https://app.zeplin.io/login");
            driver.findElement(By.id("handle")).sendKeys("bichntn4@sendo.vn");
            driver.findElement(By.id("password")).sendKeys("BichTanOanhAnh@123");
            Thread.sleep(1000);
            driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();
            Thread.sleep(3000);
            driver.navigate().to("https://app.zeplin.io/project/5d4a412481848e4bde98ee1e/screen/5e3a8f7e673bdd89bcc8ac66");
            Thread.sleep(20000);
            List<WebElement> elements = driver.findElements(By.className("zplLayer"));
            Thread.sleep(1000);
            System.out.println(elements.size());
            for(int i=0; i<elements.size(); i++){
                try{
                    System.out.println("i=" + i);
                    for(int k=1; k<=6; k++){
                        elements.get(i).click();
//                    for(int j=0; j<sheet.getRow(0).getLastCellNum(); j++){
//                        XLSWorker.updateExcel(sheet, i+1,j,"SET" + i + "-" + j);
//                    }
                        String name = getText((By.xpath("//span[@class='name ellipsis']")));
                        String width = getText(By.xpath("//span[contains(text(),'Width')]/following-sibling::span"));
                        String height = getText(By.xpath("//span[contains(text(),'Height')]/following-sibling::span"));
                        String font = getText(By.xpath("//div[@class='layerFontFace']/span"));
                        String size = getText(By.xpath("//span[contains(text(),'Size')]/following-sibling::span"));
                        String lineHeight = getText(By.xpath("//span[contains(text(),'Line Height')]/following-sibling::span"));
                        String spacing = getText(By.xpath("//span[contains(text(),'Letter Spacing')]/following-sibling::span"));
                        String color = getText(By.xpath("//div[@class='infoContent']/div[@class='value']"));
                        String text = getText(By.xpath("//p[@class='layerContent']"));
                        if(name!=null){
                            XLSWorker.updateExcel(sheet, i*6 + k,0,name);
                        }
                        XLSWorker.updateExcel(sheet, i*6 + k,13,width);
                        XLSWorker.updateExcel(sheet, i*6 + k,12,height);
                        XLSWorker.updateExcel(sheet, i*6 + k,3,font);
                        XLSWorker.updateExcel(sheet, i*6 + k,4,size);
                        XLSWorker.updateExcel(sheet, i*6 + k,6,lineHeight);
                        XLSWorker.updateExcel(sheet, i*6 + k,9,spacing);
                        XLSWorker.updateExcel(sheet, i*6 + k,10,color);
                        if(text!=null){
                            XLSWorker.updateExcel(sheet, i*6 + k,0,text);
                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        driver.close();
        XLSWorker.writeExcel(System.getProperty("user.dir") + "/output/result.xlsx" , workbook);
    }
}
