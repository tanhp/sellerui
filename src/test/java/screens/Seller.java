package screens;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.Color;
import org.testng.annotations.Test;
import utils.XLSWorker;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Seller {
    XSSFWorkbook workbook;
    XSSFSheet sheet;
    WebDriver driver;
    String sheetName = "PaymentOnline";
    String url = "http://ban-pwa.test.sendo.vn/cau-hinh/thanh-toan-online";
    boolean result = true;

    @Test
    public void checkUI(){
        try {
            System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/driver/chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-extensions");
            options.addArguments("--start-maximized");
            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            driver.get("http://ban-pwa.test.sendo.vn/");

            workbook = XLSWorker.getWorkbook(System.getProperty("user.dir") + "/input/template.xlsx");
            sheet = XLSWorker.getSheet(workbook, "Template");

            JSONObject obj = new JSONObject();
            obj.put("username", "shoplinhtinh85@gmail.com");
            obj.put("password", "tester@sendo.@");
            loginWithApi(obj.toString());
            loginWithApi(obj.toString());

            driver.navigate().to(url);
            Thread.sleep(5000);

            WebElement container = driver.findElement(By.className("main_32yy"));
            List<WebElement> elements = container.findElements(By.xpath(".//*"));
            System.out.println(elements.size());
            int j=0;
            for(int i=0; i<elements.size(); i++){
                WebElement element = elements.get(i);
                String name = element.getAttribute("id");
                String text = element.getText();
                System.out.println("Name: " + name);
                System.out.println("Text: " + text);

                if(name.equals("") && text.equals("")){
                    continue;
                }
                j++;                String width = element.getCssValue("width");
                String height = element.getCssValue("height");
                String font = element.getCssValue("font-family");
                String size = element.getCssValue("font-size");
                String fontWeight = element.getCssValue("font-weight");
                String lineHeight = element.getCssValue("line-height");
                String spacing = element.getCssValue("letter-spacing");
                String color = Color.fromString(element.getCssValue("color")).asHex();

                XLSWorker.updateExcel(sheet, j,0,name);
                XLSWorker.updateExcel(sheet, j,13,width);
                XLSWorker.updateExcel(sheet, j,12,height);
                XLSWorker.updateExcel(sheet, j,3,font);
                XLSWorker.updateExcel(sheet, j,4,size);
                XLSWorker.updateExcel(sheet, j,5,fontWeight);
                XLSWorker.updateExcel(sheet, j,6,lineHeight);
                XLSWorker.updateExcel(sheet, j,9,spacing);
                XLSWorker.updateExcel(sheet, j,10,color);
                if(name.equals("")){
                    XLSWorker.updateExcel(sheet, j,0,text);
                }

            }


        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        driver.close();
        XLSWorker.writeExcel(System.getProperty("user.dir") + "/output/seller_result.xlsx" , workbook);
    }

    private String getText(By by){
        try{
            WebElement element = driver.findElement(by);
            return element.getText();
        }catch (Exception e){
            return null;
        }

    }

    public void loginWithApi(String payload){
        String url = "http://apipwa.seller.test.sendo.vn/api/token/web";
        String response = sendApiRequest("POST", url, payload, "");
        String token = getAuthorizationCode(response);
        System.out.println(token);
        addCookie("token_seller_api",token);
        driver.navigate().refresh();
    }

    public void addCookie(String key, String value){
        try{
            Cookie cookie = new Cookie(key,value);
            driver.manage().addCookie(cookie);
        }catch (Exception e){
        }

    }

    public static String sendApiRequest(String method, String source, String payload, String authorizationCode) {
        System.out.println("************************************************");
        System.out.println("Start to request with authorization ...");
        System.out.println("Method: " + method);
        System.out.println("Source: " + source);
        System.out.println("Payload: " + payload);
        System.out.println("Token: " + authorizationCode);
        assert (source != "" && source != null) : "API link is empty";

        String response = "";
        URL url = null;
        BufferedReader bufReader = null;
        HttpURLConnection conn = null;

        try {
            url = new URL(source);
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("ReCaptchaResponse", "smc@123456");
            //conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            if(authorizationCode != null) {
                conn.setRequestProperty("Authorization", authorizationCode);
            }
            conn.setDoOutput(true);
            if(!payload.equals("")){
                DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                outStream.write(payload.getBytes());
                outStream.flush();
                outStream.close();
            }

            System.out.println("ResponseCode: " + conn.getResponseCode());
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } else {
                bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            try {
                response = bufReader.readLine();
                if (bufReader != null) {
                    bufReader.close();
                }
                conn.disconnect();
            }catch (Exception e){
                System.out.println(123454);
            }
        }
        if(!response.contains("\"")) {
            //response = StringEscapeUtils.unescapeJava(response);
        }
        System.out.println("Get the response after run: " + response);
        System.out.println("************************************************");
        return response;
    }


    public static String getAuthorizationCode(String response){
        try {
            JSONObject fullResponse = new JSONObject(response);
            JSONObject result = (JSONObject) fullResponse.get("result");
            if(result.has("token"))
                return (String) result.get("token");
            else
                return (String) result.get("access_token");
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public static By getBy(String using, String locator){
        if (using.equalsIgnoreCase("XPATH")) {
            return By.xpath(locator);
        }

        //find by class
        else if (using.equalsIgnoreCase("CLASSNAME")) {
            return By.className(locator);
        }

        //find by id
        else if (using.equalsIgnoreCase("ID")) {
            return By.id(locator);
        }

        //find by name
        else if (using.equalsIgnoreCase("NAME")) {
            return By.name(locator);
        }

        //find by css
        else if (using.equalsIgnoreCase("CSS")) {
            return By.cssSelector(locator);
        }

        //find by link
        else if (using.equalsIgnoreCase("LINKTEXT")) {
            return By.linkText(locator);
        }

        //find by partial link
        else if (using.equalsIgnoreCase("PARTIALLINK")) {
            return By.partialLinkText(locator);
        }
        return null;
    }

    public void reportToExcel(Boolean result, int row, String errorMessage){
        if (!result) {
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet) - 1, errorMessage);
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), "FAILED");
        } else {
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), "PASSED");
        }
    }

    public void writeExcel(String fileName){

    }
}
