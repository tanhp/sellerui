package screens;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.Color;
import org.testng.annotations.Test;
import utils.XLSWorker;

public class Register {
    XSSFWorkbook workbook;
    XSSFSheet sheet;
    WebDriver driver;
    String sheetName = "Register";
    boolean result = true;

    @Test
    public void checkUI(){
        try {
            System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/driver/chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-extensions");
            options.addArguments("--start-maximized");
            driver = new ChromeDriver(options);

            driver.get("https://ban.sendo.vn");

            workbook = XLSWorker.getWorkbook(System.getProperty("user.dir") + "/input/input_css.xlsx");
            sheet = XLSWorker.getSheet(workbook, sheetName);

            for(int i=1; i<sheet.getLastRowNum(); i++){
                System.out.println("Row: " + i);
                String by = XLSWorker.getValueFromExcel(i, "By", sheetName);
                String locator = XLSWorker.getValueFromExcel(i, "Locator", sheetName);
                WebElement element = driver.findElement(getBy(by, locator));
                for(int j=3; j<XLSWorker.getLastCellNum(sheet)-2; j++){
                    String cssName = XLSWorker.getValueFromExcel(0, j, sheetName);
                    String exp_value = XLSWorker.getValueFromExcel(i, j, sheetName);
                    String act_value = element.getCssValue(cssName);
                    switch (cssName){
                        case "color":
                        case "background-color":
                            act_value = Color.fromString(act_value).asHex();
                    }

                    if(!exp_value.equals("")){
                        System.out.println(cssName + ": " + exp_value + "-" + act_value);
                        if(!act_value.equals(exp_value)){
                            result = false;
                            reportToExcel(result, i, cssName + ": " + act_value);
                            break;
                        }else{
                            reportToExcel(result, i, act_value);
                        }
                    }
                }
            }


        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        driver.close();
        XLSWorker.writeExcel(System.getProperty("user.dir") + "/output/result.xlsx" , workbook);

    }

    public static By getBy(String using, String locator){
        if (using.equalsIgnoreCase("XPATH")) {
            return By.xpath(locator);
        }

        //find by class
        else if (using.equalsIgnoreCase("CLASSNAME")) {
            return By.className(locator);
        }

        //find by id
        else if (using.equalsIgnoreCase("ID")) {
            return By.id(locator);
        }

        //find by name
        else if (using.equalsIgnoreCase("NAME")) {
            return By.name(locator);
        }

        //find by css
        else if (using.equalsIgnoreCase("CSS")) {
            return By.cssSelector(locator);
        }

        //find by link
        else if (using.equalsIgnoreCase("LINKTEXT")) {
            return By.linkText(locator);
        }

        //find by partial link
        else if (using.equalsIgnoreCase("PARTIALLINK")) {
            return By.partialLinkText(locator);
        }
        return null;
    }

    public void reportToExcel(Boolean result, int row, String errorMessage){
        if (!result) {
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet) - 1, errorMessage);
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), "FAILED");
        } else {
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), "PASSED");
        }
    }

    public void writeExcel(String fileName){

    }
}
