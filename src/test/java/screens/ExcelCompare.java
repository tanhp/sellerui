package screens;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.support.Color;
import org.testng.annotations.Test;
import utils.XLSWorker;

import java.time.Duration;
import java.time.Instant;

public class ExcelCompare {
    XSSFWorkbook workbook1, workbook2;
    XSSFSheet sheet1, sheet2;
    String feature = "PaymentOnline";
    boolean result = true;
    String message;

    @Test
    public void checkUI(){
        Instant start = Instant.now();

        // Tên file trang bán
        workbook1 = XLSWorker.getWorkbook(System.getProperty("user.dir") + "/output/seller_result.xlsx");
        // Tên file zeplin
        workbook2 = XLSWorker.getWorkbook(System.getProperty("user.dir") + "/output/zeplin_result.xlsx");

        sheet1 = XLSWorker.getSheet(workbook1, "Template");
        sheet2 = XLSWorker.getSheet(workbook2, "Template");

        for(int i=1; i<sheet1.getLastRowNum(); i++){
            String name1 = XLSWorker.getValueFromExcel(workbook1, i, "Element", "Template");
            for(int j=1; j<sheet2.getLastRowNum(); j++){
                message = "";
                System.out.println("i=" + i + "-" + "j=" + j);

                String name2 = XLSWorker.getValueFromExcel(workbook2, j, "Element", "Template");
                System.out.println("name1=" + name1 + "-" + "name2=" + name2);
                result = true;

                if(name1.equals(name2)){
                    for(int k=3; k<XLSWorker.getLastCellNum(sheet1)-2; k++){



                        String value1 = XLSWorker.getValueFromExcel(workbook1, i, k, "Template");
                        String value2 = XLSWorker.getValueFromExcel(workbook2, j, k, "Template");

                        String header = XLSWorker.getValueFromExcel(0, k, "Template");
                        switch (header){
                            case "color":
                            case "background-color":
                                if(value1.contains("rgb")){
                                    value1 = Color.fromString(value1).asHex();
                                }
                                if(value2.contains("rgb")){
                                    value2 = Color.fromString(value1).asHex();
                                }

                        }
                        System.out.println("Value" + ": " + value1 + "-" + value2);
                        if(!value1.equals("") || !value2.equals("")){
                            if(header.equals("font-family")){
                                if(!value1.contains(value2)) {
                                    result = false;
                                    message = message + header + ": " + value2 + ", ";
                                    reportToExcel(result, i, message);
                                }
                                else
                                    reportToExcel(result, i, "");
                            }else{
                                if(!header.equals("width") && !header.equals("height")){
                                    if(!value1.equals(value2)){
                                        result = false;
                                        message = message + header + ": " + value2 + ", ";
                                        reportToExcel(result, i, message);
                                    }else{
                                        reportToExcel(result, i, message);
                                    }
                                }

                            }

                        }
                    }
                }
            }
        }

        XLSWorker.writeExcel(System.getProperty("user.dir") + "/output/result_" + feature + ".xlsx" , workbook1);

        Instant end = Instant.now();
        Duration interval = Duration.between(start, end);
        System.out.println("Execution time in seconds: " + interval.getSeconds());
    }

    public void reportToExcel(Boolean result, int row, String errorMessage){
        System.out.println(result + "-" + errorMessage);
        if (!result) {
            XLSWorker.updateExcel(sheet1, row, XLSWorker.getLastCellNum(sheet1) - 1, errorMessage);
            XLSWorker.updateExcel(sheet1, row, XLSWorker.getLastCellNum(sheet1), "FAILED");
        } else {
            XLSWorker.updateExcel(sheet1, row, XLSWorker.getLastCellNum(sheet1), "PASSED");
        }

    }
}
