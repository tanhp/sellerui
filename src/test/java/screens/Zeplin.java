package screens;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;
import utils.XLSWorker;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class Zeplin {
    XSSFWorkbook workbook;
    XSSFSheet sheet;
    WebDriver driver;
    String url = "https://app.zeplin.io/project/5d4a412481848e4bde98ee1e/screen/5e7459def22821bc541ada4f";

    private String getText(By by){
        try{
            WebElement element = driver.findElement(by);
            return element.getText();
        }catch (Exception e){
            return null;
        }

    }

    private String getValue(By by){
        try{
            WebElement element = driver.findElement(by);
            return element.getAttribute("value");
        }catch (Exception e){
            return null;
        }

    }

    @Test
    public void runZeplin(){
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/driver/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);

        workbook = XLSWorker.getWorkbook(System.getProperty("user.dir") + "/input/template.xlsx");
        sheet = XLSWorker.getSheet(workbook, "Template");

        try{
            JavascriptExecutor js = (JavascriptExecutor)driver;
            driver.get("https://app.zeplin.io/login");
            driver.findElement(By.id("handle")).sendKeys("bichntn4@sendo.vn");
            driver.findElement(By.id("password")).sendKeys("BichTanOanhAnh@123");
            sleep(1000);
            driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();
            sleep(3000);
            driver.navigate().to(url);
            sleep(20000);
            List<WebElement> elements = driver.findElements(By.className("zplLayer"));
            sleep(1000);
            System.out.println(elements.size());
            int j=0;
            for(int i=0; i<elements.size(); i++){
                try{
                    System.out.println("i=" + i);
                    for(int k=1; k<=6; k++){
                        elements.get(i).click();
//                    for(int j=0; j<sheet.getRow(0).getLastCellNum(); j++){
//                        XLSWorker.updateExcel(sheet, i+1,j,"SET" + i + "-" + j);
//                    }
                        Thread.sleep(250);
                        String name = getText((By.xpath("//span[@class='name ellipsis']")));
                        if(name!=null){
                            String[] a = name.split("/ ");
                            name = a[a.length-1];
                        }
                        String text = getText(By.xpath("//h2[@class='title']/following-sibling::p"));
                        if(name==null && text==null)
                            continue;
                        j++;
                        String width = getText(By.xpath("//span[@class='token property' and text()='width']/following-sibling::span[@class='token number']"));
                        String height = getText(By.xpath("//span[@class='token property' and text()='height']/following-sibling::span[@class='token number']"));
                        String font = getText(By.xpath("//span[@class='token property' and text()='font-family']/parent::pre"));
                        if(font!=null){
                            font = font.replace(" ", "").replace(";", "").replace("font-family:", "");
                        }
                        String fontWeight = getText(By.xpath("//span[@class='token property' and text()='font-weight']/following-sibling::span[@class='token number' or @class='token builtin']"));
                        if(fontWeight!=null && fontWeight.equals("normal"))
                            fontWeight = "400";
                        else if (fontWeight!=null && fontWeight.equals("bold"))
                            fontWeight = "700";
                        String size = getText(By.xpath("//span[@class='token property' and text()='font-size']/following-sibling::span[@class='token number']"));
                        String lineHeight = getText(By.xpath("//span[text()='Line Height: ']/following-sibling::span"));
                        String spacing = getText(By.xpath("//span[@class='token property' and text()='letter-spacing']/following-sibling::span[@class='token number' or @class='token builtin']"));
                        String color = getText(By.xpath("//div[@class='infoContent']/div[@class='value']"));

                        System.out.println("j=" + j);
                        System.out.println("Name: " + name);
                        System.out.println("Text: " + text);
                        XLSWorker.updateExcel(sheet, j,0,name);
                        XLSWorker.updateExcel(sheet, j,13,width);
                        XLSWorker.updateExcel(sheet, j,12,height);
                        XLSWorker.updateExcel(sheet, j,3,font);
                        XLSWorker.updateExcel(sheet, j,4,size);
                        XLSWorker.updateExcel(sheet, j,5,fontWeight);
                        XLSWorker.updateExcel(sheet, j,6,lineHeight);
                        XLSWorker.updateExcel(sheet, j,9,spacing);
                        XLSWorker.updateExcel(sheet, j,10,color);
                        if(name==null){
                            XLSWorker.updateExcel(sheet, j,0,text);
                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        driver.close();
        XLSWorker.writeExcel(System.getProperty("user.dir") + "/output/zeplin_result.xlsx" , workbook);
    }
}
