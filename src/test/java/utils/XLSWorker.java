package utils;

import org.apache.poi.xssf.usermodel.*;
import org.testng.TestException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XLSWorker {
    private static FileInputStream fis = null;
    private static FileOutputStream fos = null;
    private static XSSFWorkbook workbook = null;
    private static XSSFSheet sheet = null;
    private XSSFRow row = null;
    private XSSFCell cell = null;
    private static String sheetName = null;

    public static List<String> FSheets = new ArrayList<String>();
    public static List<Integer>FRows = new ArrayList<Integer>();
    public static List<Integer>FCols = new ArrayList<Integer>();
    public static List<String>FUrl = new ArrayList<String>();

    public XLSWorker(){

    }

    public static XSSFWorkbook createWorkbook(){
        try{
            return new XSSFWorkbook();
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    public static XSSFSheet createSheet(XSSFWorkbook workbook, String sheetName){
        try{
            if(workbook==null)
                return null;
            return workbook.createSheet(sheetName);
        }catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    public static XSSFWorkbook getWorkbook(String filePath){
        try {
            fis = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(fis);
            //sheet = workbook.getSheetAt(0);
            fis.close();
            return workbook;
        } catch (Exception e) {
            throw new TestException(e);
        }
    }

    public static XSSFSheet getSheet(XSSFWorkbook workbook, String sheetName){
        if(workbook==null)
            return null;
        try{
            return workbook.getSheetAt(workbook.getSheetIndex(sheetName));
        }catch (Exception e) {
            throw new TestException(e);
        }
    }

    public static String getValueFromExcel(int row, String colName, String sheetName){
        int colNum = XLSWorker.getColumnNumByName(colName, sheetName);
        Object[][] object = XLSWorker.getData(workbook,sheetName);
        if(object == null)
            return null;
        return object[row][colNum].toString();
    }

    public static String getValueFromExcel(XSSFWorkbook workbook, int row, String colName, String sheetName){
        int colNum = XLSWorker.getColumnNumByName(colName, sheetName);
        Object[][] object = XLSWorker.getData(workbook,sheetName);
        if(object == null)
            return null;
        return object[row][colNum].toString();
    }

    public static String getValueFromExcel(int row, int col, String sheetName){
        Object[][] object = XLSWorker.getData(workbook,sheetName);
        if(object == null)
            return null;
        return object[row][col].toString();
    }

    public static String getValueFromExcel(XSSFWorkbook workbook, int row, int col, String sheetName){
        Object[][] object = XLSWorker.getData(workbook,sheetName);
        if(object == null)
            return null;
        return object[row][col].toString();
    }

    public static int getColumnNumByName(String colName, String sheetName){
        XSSFSheet sheet = XLSWorker.getSheet(workbook,sheetName);
        XSSFRow row = sheet.getRow(0);
        int colNum = -1;
        for(int i=0; i < row.getLastCellNum();i++){
            if(row.getCell(i).getStringCellValue().trim().equals(colName))
                colNum = i;
        }
        return colNum;
    }

    public static int getLastCellNum(XSSFSheet sheet){
        return sheet.getRow(0).getLastCellNum()-1;
    }

    public static void writeExcel(String filePath, XSSFWorkbook workbook){
        try{
            fos = new FileOutputStream(new File(filePath));
            workbook.write(fos);
            fos.close();
            System.out.println("Excel written completed. Please chect at: " + filePath);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void updateExcel(XSSFSheet sheet, int rowNum, int colNum, String data){
        if(data!=null){
            XSSFCell cell = null;
            XSSFRow row = sheet.getRow(rowNum);
            if(row == null)
                row = sheet.createRow(rowNum);
            cell = row.getCell(colNum);
            if(cell == null){
                cell = row.createCell(colNum);
            }
            cell.setCellValue(data);
        }
    }

    public static Object[][] getData(XSSFWorkbook workbook, String sheetName){
        Object[][] objects = null;

        XSSFSheet actionSheet = XLSWorker.getSheet(workbook,sheetName);
        if(actionSheet==null)
            return null;
        int totalRows = actionSheet.getLastRowNum() - actionSheet.getFirstRowNum() + 1;
        int totalCols = actionSheet.getRow(0).getLastCellNum();
        objects = new Object[totalRows][totalCols];

        for(int i=0; i< totalRows; i++){
            for(int j=0; j < totalCols; j++){
                if(actionSheet.getRow(i).getCell(j)==null){
                    actionSheet.getRow(i).createCell(j);
                    actionSheet.getRow(i).getCell(j).setCellValue("");
                }

                objects[i][j] = actionSheet.getRow(i).getCell(j).toString();
            }
        }
        return objects;
    }
}
